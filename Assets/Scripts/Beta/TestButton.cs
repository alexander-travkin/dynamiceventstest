﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Varwin;

namespace Beta
{
    public class TestButton : Wrapper
    {
        private string _displayText;
        
        public string DisplayText
        {
            get => _displayText;
            set
            {
                _displayText = value;
                Debug.Log("TestButton Display text setted to " + value);
            }
        }
        
        public delegate void VoidVoidEventHandler();
        public delegate void VoidIntEventHandler(int a);
        
        public event EventHandler SimpleEvent;
        public event EventHandler<int> IntEvent;

        public event VoidVoidEventHandler TabPressed;
        public event VoidIntEventHandler SpacePressed;

        private int _spacePressedCount = 0;
        
        // Start is called before the first frame update
        private void Awake()
        {
            Wrapper.Collection.Add(1, this);
        }

        // Update is called once per frame
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                TabPressed?.Invoke();
                SimpleEvent?.Invoke(this, null);
            }
            
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _spacePressedCount++;
                SpacePressed?.Invoke(_spacePressedCount);
                IntEvent?.Invoke(this, _spacePressedCount);
            }
        }
    }
}