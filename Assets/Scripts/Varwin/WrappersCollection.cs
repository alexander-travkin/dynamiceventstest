using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Varwin
{
    public class WrappersCollection
    {
        private readonly Dictionary<int, Wrapper> _wrappers = new Dictionary<int, Wrapper>();
        
        public void Add(int idInstance, Wrapper wrapper)
        {
            _wrappers.Add(idInstance, wrapper);
        }
        
        public List<Wrapper> Wrappers()
        {
            List<Wrapper> result = new List<Wrapper>();
            result.AddRange(_wrappers.Values);
            return result;
        }
        
        public dynamic Get(int id)
        {
            if (_wrappers.ContainsKey(id))
            {
                return _wrappers[id];
            }
            throw new Exception($"Wrapper with id = {id} not found!");
        }
        
        public dynamic Get(string typeName)
        {
            foreach (var wrapper in _wrappers)
            {
                if (wrapper.Value.GetType().FullName == typeName)
                {
                    return wrapper.Value;
                }
            }
            
            throw new Exception($"Wrapper with typeName = {typeName} not found!");
        }
    }

}
