﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using Varwin;

namespace Alpha
{
    public class Main : MonoBehaviour
    {
        public ILogic Logic = new LogicOfLocation52();

        private void Start()
        {
            Logic.SetCollection(Wrapper.Collection);
            
            Logic.Initialize();

            Logic.Events();
        }
    }
}