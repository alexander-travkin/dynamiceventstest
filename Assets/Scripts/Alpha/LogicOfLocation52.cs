using System;
using UnityEngine;
using Varwin;

namespace Alpha
{
    public class LogicOfLocation52 : ILogic
    {
        private WrappersCollection collection;

        public void SetCollection(WrappersCollection collection)
        {
            this.collection = collection;
        }

        public void Initialize()
        {
            (collection.Get(1)).DisplayText = "Hello World!";
        }

        public void Update()
        {
        }

        public void Events()
        {
            dynamic a = collection.Get(1);
            ILogicExtensions.AddEventHandler(this, a, "SimpleEvent");
            ILogicExtensions.AddEventHandler(this, a, "IntEvent");
            ILogicExtensions.AddEventHandler(this, a, "TabPressed");
            ILogicExtensions.AddEventHandler(this, a, "SpacePressed");
        }
        
        private void OnSimpleEvent(object sender, EventArgs args)
        {
            Debug.Log($"OnSimpleEvent({sender}, {args})");
        }

        private void OnIntEvent(object sender, int count)
        {
            Debug.Log($"OnIntEvent({sender}, {count})");
        }

        private void OnTabPressed()
        {
            Debug.Log($"OnTabPressed");
        }

        private void OnSpacePressed(int count)
        {
            Debug.Log($"OnSpacePressed({count})");
        }
    }
}